
import java.util.Scanner;


public class Exercici1 {
    
    public static void main(String[] args){
        
        System.out.print("Entra una paraula:");
        Scanner entrada = new Scanner(System.in);
        String paraulaACercar = entrada.nextLine();
        System.out.print("Entra una frase:");
        String paraulaOnCercar = entrada.nextLine();
        int numAparicions = 0;
        int posicio = 0;
        while ((posicio = paraulaOnCercar.indexOf(paraulaACercar,posicio)) != -1){
            posicio++;
            numAparicions++;
        }
        System.out.println(numAparicions);
    }
}
